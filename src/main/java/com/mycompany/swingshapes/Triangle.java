/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.swingshapes;

/**
 *
 * @author ACER
 */
public class Triangle extends Shape {

    private double base;
    private double height;
    private double opposite;

    public Triangle(double base, double height) {
        super("Triangle");
        this.base = base;
        this.height = height;
        this.opposite = (Math.sqrt((Math.pow(height, 2)) + (Math.pow(base, 2))));
    }

    public double getBase() {
        return base;
    }

    public void setBase(double base) {
        this.base = base;
    }

    public double getHeight() {
        return height;
    }

    public double getOpposite() {
        return opposite;
    }

    public void setOpposite(double opposite) {
        this.opposite = opposite;
    }

    public void setHeight(double height) {
        this.height = height;
    }

    @Override
    public double calArea() {
        return 0.5 * base * height;
    }

    @Override
    public double calPerimeter() {
        return opposite + base + height;
    }

}
