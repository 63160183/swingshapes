/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.swingshapes;

import java.awt.Color;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JTextField;

/**
 *
 * @author ACER
 */
public class TriangleFrame{        
    public static void main(String[] args) {
        final JFrame frame = new JFrame("Triangle");
        frame.setSize(500,500);
        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        frame.setLayout(null);
        
        JLabel lblBase= new JLabel("Base:", JLabel.TRAILING);
        lblBase.setSize(50, 20);
        lblBase.setLocation(5, 5);
        lblBase.setBackground(Color.WHITE);
        lblBase.setOpaque(true);
        frame.add(lblBase);
        
        final JTextField txtBase = new JTextField();
        txtBase.setSize(50, 20);
        txtBase.setLocation(60, 5);
        frame.add(txtBase);
        
        JLabel lblHigh = new JLabel("Hight:",JLabel.TRAILING);
        lblHigh.setSize(100, 20);
        lblHigh.setLocation(120, 5);
        lblBase.setBackground(Color.WHITE);
        lblBase.setOpaque(true);
        frame.add(lblHigh);
        
        final JTextField txtHigh = new JTextField();
        txtHigh.setSize(100, 20);
        txtHigh.setLocation(230, 5);
        frame.add(txtHigh);
        
        JButton btnCalculate = new JButton("Calculate");
        btnCalculate.setSize(100, 20);
        btnCalculate.setLocation(340, 5);
        frame.add(btnCalculate);
        
        final JLabel lblResult = new JLabel("Triangle base= ??? high= ??? area= ??? opposite= ??? perimeter= ???");
        lblResult.setHorizontalAlignment(JLabel.CENTER);
        lblResult.setSize(500, 50);
        lblResult.setLocation(0, 50);
        lblResult.setBackground(Color.pink);
        lblResult.setOpaque(true);
        frame.add(lblResult);
        
        btnCalculate.addActionListener(new ActionListener(){
            @Override
            public void actionPerformed(ActionEvent arg0) {
                try{
                    String strBase = txtBase.getText();
                    double Base = Double.parseDouble(strBase);
                    String strHigh = txtHigh.getText();
                    double High = Double.parseDouble(strHigh);
                    Triangle triangle = new Triangle(Base, High);
                    lblResult.setText("Triangle base = "+String.format("%.2f", triangle.getBase())
                            +" height = "+String.format("%.2f", triangle.getHeight())
                            + " area = "+String.format("%.2f", triangle.calArea()) 
                            + " opposite = "+String.format("%.2f", triangle.getOpposite())
                            + " perimeter = "+String.format("%.2f", triangle.calPerimeter()));
                }catch(Exception ex1){
                    JOptionPane.showMessageDialog(frame, "Error: Please input number"
                            , "Error", JOptionPane.ERROR_MESSAGE);
                    txtBase.setText("");
                    txtHigh.setText("");
                    txtBase.requestFocus();
                }
            }
            
        });
        
        frame.setVisible(true);
    }
}
